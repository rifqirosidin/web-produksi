<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('orders', 'Api\OrderController@index');
Route::post('orders', 'Api\OrderController@store');
Route::get('orders/{id}/edit', 'Api\OrderController@edit');
Route::put('orders/update/{id}', 'Api\OrderController@update');
Route::delete('orders/delete/{id}', 'Api\OrderController@destroy');


