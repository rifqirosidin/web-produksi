<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/admin', function () {
    return view('layouts.vali');
});

Route::resource('order', 'OrdersController');
Route::resource('category', 'CategoryController');
Route::resource('vendors', 'VendorController');
Route::resource('request-order', 'RequestOrderController');
Route::put('request-order/update/{id}', 'RequestOrderController@update')->name('approve.order');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
