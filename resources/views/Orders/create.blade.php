@extends('dashboard.layout')
@section('header')
    Buat Order
@endsection
@section('content-dashboard')

    <div class="row">
        <div class="col-md-6">
            <div class="tile">
                <form action="{{ route('order.store') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="nama_barang">Tanggal Beli</label>
                        <input class="form-control" id="nama_barang" name="tgl_beli" type="date" >
                    </div>
                    <div class="form-group">
                        <label for="nama_barang">Nama Barang</label>
                        <input class="form-control" id="nama_barang" name="nama_barang" type="text" >
                    </div>
                    <div class="form-group">
                        <label for="jumlah">Jumlah</label>
                        <input class="form-control" id="jumlah" name="jumlah" type="number" >
                    </div>
                    <div class="form-group">
                        <label for="harga">Harga</label>
                        <input class="form-control" id="harga" name="harga" type="number" >
                    </div>
                    <div class="form-group">
                        <label for="category_id">Kategori</label>
                        <select name="category_id" class="form-control"  id="category_id">
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="vendor_id">Vendor</label>
                        <select name="vendor_id" class="form-control"  id="vendor_id">
                            @foreach($vendors as $vendor)
                                <option value="{{ $vendor->id }}">{{ $vendor->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="vendor_id">Status</label>
                        <select name="status" class="form-control"  id="vendor_id">
                            <option value="order">Order</option>
                            <option value="tersedia">Tersedia</option>
                            <option value="habis">Habis</option>
                        </select>
                    </div>


                    <div class="tile-footer ">
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
