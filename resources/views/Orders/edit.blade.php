@extends('dashboard.layout')
@section('header')
    Edit Order - {{ $orders->nama_barang }}
@endsection
@section('content-dashboard')

    <div class="row">
        <div class="col-md-6">
            <div class="tile">
                <form action="{{ route('order.update', $orders->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="nama_barang">Nama Barang</label>
                        <input class="form-control" value="{{ $orders->nama_barang }}" id="nama_barang" name="nama_barang" type="text"  placeholder="Enter name">
                    </div>
                    <div class="form-group">
                        <label for="jumlah">Jumlah</label>
                        <input class="form-control" value="{{ $orders->jumlah }}"  id="jumlah" name="jumlah" type="text"  placeholder="Enter name">
                    </div>
                    <div class="form-group">
                        <label for="harga">Harga</label>
                        <input class="form-control" id="harga" value="{{ $orders->harga }}"  name="harga" type="number"  placeholder="Enter name">
                    </div>

                    <div class="form-group">
                        <label for="category_id">Kategori</label>
                        <select name="category_id" class="form-control"  id="category_id">
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}"
                                    {{ $orders->category_id == $category->id ? 'selected':'' }}
                                >{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="vendor_id">Vendor</label>
                        <select name="vendor_id" class="form-control"  id="vendor_id">
                            @foreach($vendors as $vendor)
                                <option value="{{ $vendor->id }}"
                                    {{ $orders->vendor_id == $vendor->id ? 'selected':'' }}
                                >{{ $vendor->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="vendor_id">Status</label>
                        <select name="status" class="form-control"  id="vendor_id">
                                <option value="order">Order</option>
                                <option value="tersedia">Tersedia</option>
                                <option value="habis"></option>
                        </select>
                    </div>

                    <div class="tile-footer">
                        <button class="btn btn-primary" type="submit">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
