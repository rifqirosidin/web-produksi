@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>
                    <script>
                        $.notify({
                            title: "Update Complete : ",
                            message: "{{ $error }}",
                            icon: 'fa fa-check'
                        },{
                            type: "warning"
                        });

                    </script>
                </li>
            @endforeach
        </ul>
    </div>
@endif
