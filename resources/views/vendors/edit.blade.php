@extends('dashboard.layout')
@section('header')
    Edit Vendor - {{ $vendor->nama }}
@endsection
@section('content-dashboard')

    <div class="row">
        <div class="col-md-6">
            <div class="tile">
                <form action="{{ route('vendors.update', $vendor->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nama Vendor</label>
                        <input class="form-control" id="name" value="{{ $vendor->nama }} " name="nama" type="text"  placeholder="Enter name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Alamat</label>
                        <textarea name="alamat" id="" cols="20" rows="10" class="form-control">
                            {{ $vendor->alamat }}
                        </textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">No Hp</label>
                        <input class="form-control" id="name" value="{{ $vendor->nohp }}" name="nohp" type="text"  placeholder="Enter name">
                    </div>

                    <div class="tile-footer">
                        <button class="btn btn-primary" type="submit">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
