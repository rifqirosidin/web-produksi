@extends('dashboard.layout')
@section('header')
    Daftar Vendor
@endsection
@section('content-dashboard')

    <div class="row">
        <div class="col-md-12">

            <div class="tile">
                <div class="row">
                    <div class="col-md-12 mb-2">
                        <div class="float-right">
                            <a href="{{ route('vendors.create') }}" class="btn btn-outline-primary">Add Vendor</a>
                        </div>
                    </div>
                </div>
                <div class="tile-body">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Alamat</th>
                            <th>No Hp</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($vendors as $vendor)
                            <tr>
                                <td width="10%">{{ $loop->iteration }}</td>
                                <td>{{ $vendor->nama }}</td>
                                <td>{{ $vendor->alamat }}</td>
                                <td>{{ $vendor->nohp }}</td>
                                <td width="25%">

                                    <a href="{{ route('vendors.edit', $vendor->id) }}" class="btn btn-sm badge-success">Edit</a>
                                    <button type="button" onclick="deleteItem('{{ $vendor->id }}')" id="delete"
                                            class="btn btn-sm badge-danger">Delete
                                    </button>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4" class="text-center">Tidak ada data</td>
                            </tr>
                        @endforelse

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @push('js')
        <script src="{{ asset('vendor/js/plugins/pace.min.js') }}"></script>
        <!-- Page specific javascripts-->
        <!-- Data table plugin-->
        <script type="text/javascript" src="{{ asset('vendor/js/plugins/jquery.dataTables.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('vendor/js/plugins/dataTables.bootstrap.min.js') }}"></script>
        <script type="text/javascript">$('#sampleTable').DataTable();</script>
        <script>

            function deleteItem(id) {
                let theUrl = "{{ route('vendors.destroy', ':id_vendor') }}"
                theUrl = theUrl.replace(':id_vendor', id);

                console.log(theUrl);
                swal({
                    title: "Apakah anda yakin?",
                    text: "Menghapus file ini",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel plx!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                }, function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: "POST",
                            url: theUrl,
                            data: {
                                "_method": 'DELETE',
                            },
                            success: function (data) {
                                swal("Deleted!", "Data Sukses di delete", "success");
                                window.location.reload()
                            },
                            error: function (data) {
                                console.log(data);
                                swal("Failed", "File gagal di delete)", "error");
                            }
                        })

                    } else {
                        swal("Cancelled", "File gagal di delete)", "error");
                    }
                });
            }

        </script>
    @endpush
@endsection
