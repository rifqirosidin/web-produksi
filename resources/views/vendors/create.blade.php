@extends('dashboard.layout')
@section('header')
    Buat Vendor
@endsection
@section('content-dashboard')

    <div class="row">
        <div class="col-md-6">
            <div class="tile">
                <form action="{{ route('vendors.store') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nama Vendor</label>
                        <input class="form-control" id="name" name="nama" type="text"  placeholder="Enter name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Alamat</label>
                        <textarea name="alamat" id="" cols="20" rows="5" class="form-control">

                        </textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">No Hp</label>
                        <input class="form-control" id="name" name="nohp" type="text"  placeholder="Enter name">
                    </div>

                    <div class="tile-footer">
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
