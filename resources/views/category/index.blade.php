@extends('dashboard.layout')
@section('content-dashboard')

    <div class="row">
        <div class="col-md-12">

            <div class="tile">
                <div class="row">
                    <div class="col-md-12 mb-2">
                        <div class="float-right">
                            <a href="{{ route('category.create') }}" class="btn btn-outline-primary">Add Category</a>
                        </div>
                    </div>
                </div>
                <div class="tile-body">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($categories as $category)
                        <tr>
                            <td width="10%">{{ $loop->iteration }}</td>
                            <td>{{ $category->name }}</td>
                            <td width="25%">

                                <a href="{{ route('category.edit', $category->id) }}" class="btn btn-sm badge-success">Edit</a>
                                <button type="button" onclick="deleteItem({{ $category->id }})" class="btn btn-sm badge-danger">Delete</button>
                            </td>
                        </tr>
                            @empty
                        <tr>
                            <td colspan="3" class="text-center">Tidak ada data</td>
                        </tr>
                            @endforelse

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @push('js')
        <script src="{{ asset('vendor/js/plugins/pace.min.js') }}"></script>
        <!-- Page specific javascripts-->
        <!-- Data table plugin-->
        <script type="text/javascript" src="{{ asset('vendor/js/plugins/jquery.dataTables.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('vendor/js/plugins/dataTables.bootstrap.min.js') }}"></script>
        <script type="text/javascript">$('#sampleTable').DataTable();</script>

        <script>
            function deleteItem(id) {
                let theUrl = "{{ route('category.destroy', ':id_category') }}"
                theUrl = theUrl.replace(':id_category', id);

                swal({
                    title: "Apakah anda yakin?",
                    text: "Menghapus file ini",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel plx!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                }, function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: "POST",
                            url: theUrl,
                            data: {
                                "_method": 'DELETE',
                            },
                            success: function (data) {
                                swal("Deleted!", "Data Sukses di delete", "success");
                                window.location.reload()
                            },
                            error: function (data) {
                                console.log(data);
                                swal("Failed", "File gagal di delete)", "error");
                            }
                        })

                    } else {
                        swal("Cancelled", "File gagal di delete)", "error");
                    }
                });
            }
        </script>
    @endpush
@endsection
