@extends('dashboard.layout')
@section('content-dashboard')

    <div class="row">
        <div class="col-md-6">
            <div class="tile">
                <form action="{{ route('category.update', $category->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="exampleInputEmail1">Edit Category Name</label>
                        <input class="form-control" id="name" value="{{ $category->name }}" name="name" type="text"  placeholder="Enter name">
                    </div>

                    <div class="tile-footer">
                        <button class="btn btn-primary" type="submit">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
