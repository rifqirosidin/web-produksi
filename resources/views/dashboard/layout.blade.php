@extends('layouts.vali')
@section('title')
Admin Dashboard Produksi
@endsection
@section('dashboard')

    <header class="app-header"><a class="app-header__logo" href="/">Admin Produksi</a>
        <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
        <!-- Navbar Right Menu-->
        <ul class="app-nav">


            <!-- User Menu-->
            <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
                <ul class="dropdown-menu settings-menu dropdown-menu-right">
                    <li><a class="dropdown-item"  href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('form-logout').submit()"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>

                </ul>
            </li>
            <form action="{{ route('logout') }}" method="post" id="form-logout">
                @csrf
            </form>
        </ul>
    </header>
    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
        <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg" alt="User Image">
            <div>
                <p class="app-sidebar__user-name">Produksi</p>
                <p class="app-sidebar__user-designation">Administrator</p>
            </div>
        </div>
        <ul class="app-menu">
            <li><a class="app-menu__item " href="{{ route('home') }}"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span></a></li>
            <li><a class="app-menu__item " href="{{ route('order.index') }}"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Order</span></a></li>
            <li><a class="app-menu__item " href="{{ route('vendors.index') }}"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Vendor</span></a></li>
            <li><a class="app-menu__item " href="{{ route('category.index') }}"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Category</span></a></li>
            <li><a class="app-menu__item " href="{{ route('request-order.index') }}"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Request Order</span></a></li>
        </ul>
    </aside>
    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><i class="fa fa-dashboard"></i>@yield('header', 'Dashboard')</h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            </ul>
        </div>
        <div class="container">
            @yield('content-dashboard')
        </div>
    </main>



@endsection
