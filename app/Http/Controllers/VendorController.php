<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreVendorPost;
use App\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vendors = Vendor::latest()->get();


        return view('vendors.index', compact('vendors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vendors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreVendorPost $request)
    {
        $validated = $request->validated();
        try {
            Vendor::create($validated);
            Session::flash('success', 'Create Vendor Success');
            return redirect()->route('vendors.index');
        } catch (\Exception $exception) {
            Session::flash('error', 'Created Vendor Failed');
            return redirect()->route('vendors.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function show(Vendor $vendor)
    {
        return view('vendors.show', compact('vendor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function edit(Vendor $vendor)
    {
        return view('vendors.edit', compact('vendor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function update(StoreVendorPost $request, Vendor $vendor)
    {
        $validated = $request->validated();
        try {
            $vendor->update($validated);
            Session::flash('success', 'Update Vendor Success');
            return redirect()->route('vendors.index');
        } catch (\Exception $exception) {
            Session::flash('error', 'Update Vendor Failed');
            return redirect()->route('vendors.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vendor = Vendor::find($id);
        try {
            $vendor->delete();
            Session::flash('success', 'Delete Vendor Success');
            return redirect()->route('vendors.index');
        } catch (\Exception $exception) {
            Session::flash('error', 'Delete Vendor Failed');
            return redirect()->route('vendors.index');
        }
    }
}
