<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\StoreOrderPost;
use App\Orders;
use App\Vendor;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Orders::with(['category', 'vendor'])->latest()->get();

        return view('orders.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::latest()->get();
        $vendors = Vendor::latest()->get();

        return view('orders.create', compact('categories', 'vendors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOrderPost $request)
    {
        $validated = $request->validated();
        $random = rand(10, 1000);
        $validated['order_number'] = 'PO' . $random . date('y');

        try {
            Orders::create($validated);
            Session::flash('success', 'Create Order Success');
            return redirect()->route('order.index');
        }catch (\Exception $exception) {
            Session::flash('error', 'Create Order Gagal');
            return redirect()->route('order.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Orders  $postOrders
     * @return \Illuminate\Http\Response
     */
    public function show(Orders $postOrders)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Orders  $postOrders
     * @return \Illuminate\Http\Response
     */
    public function edit(Orders $order)
    {
        $orders = Orders::find($order->id);
        $categories = Category::latest()->get();
        $vendors = Vendor::latest()->get();

//        return $orders;

        return view('orders.edit', compact('orders', 'categories', 'vendors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Orders  $postOrders
     * @return \Illuminate\Http\Response
     */
    public function update(StoreOrderPost $request, $id)
    {

        $validated = $request->validated();
        try {
           $order = Orders::find($id);
               $order->update($validated);
            Session::flash('success', 'Update Order Success');
            return redirect()->route('order.index');
        }catch (\Exception $exception) {
            Session::flash('error', 'Update Order Gagal');
            return redirect()->route('order.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Orders  $postOrders
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Orders::find($id);

        try {
            $order->delete();
            Session::flash('success', 'Delete Order Success');
            return redirect()->route('order.index');
        }catch (\Exception $exception) {
            Session::flash('error', 'Delete Order Gagal');
            return redirect()->route('order.index');
        }
    }
}
