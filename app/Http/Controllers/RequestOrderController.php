<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;

class RequestOrderController extends Controller
{
    protected $BASE_URL = "http://localhost:8000/api/";

    public function index()
    {
        $client = new Client();

        $url = $client->get($this->BASE_URL . "request-orders");

        $response = $url->getBody();
        $data = json_decode($response, true);

        return view('request_order.index', ['data' => $data['data']]);

    }

    public function update(Request $request, $id)
    {

        $client = new Client();

        if ($request->ajax()) {
            $status = $request->get('status');
        }


        try {
            $response = $client->put($this->BASE_URL . "request-orders/update/" . $id, [
                'header' => [
                    'Content-Type' => 'application/x-www-form-urlencoded'
                ],
                'form_params' => [
                    'status' => $status
                ]
            ]);

            return redirect()->back();

        } catch (RequestException $exception) {
            return $exception->getMessage();
        }
    }
}
