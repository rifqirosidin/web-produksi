<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\StoreOrderPost;
use App\Orders;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Orders::latest()->get();

        try {
            $data = [
                'code' => 200,
                'status' => 'success',
                'data' => $orders
            ];
            return response()->json($data);

        }catch (\Exception $exception){
            $data = [
                'code' => 404,
                'status' => 'failed',
            ];
            return response()->json($data);
        }
    }

    public function store(StoreOrderPost $request)
    {
        $validated = $request->validated();
        $random = rand(10, 1000);
        $validated['order_number'] = 'PO' . $random . date('y');
        try {
            Orders::create($validated);
            $data = [
              'code' => '401',
              'status' => 'Create Order Successfully',
              'data' => $validated
            ];

            return response()->json($data);
        } catch (\Exception $exception) {
            $data = [
                'code' => '400',
                'status' => 'Failed',
                'data' => $validated
            ];

            return response()->json($data);
        }
    }

    public function edit(Orders $orders)
    {
        try {
            $data = [
              'code' => '200',
              'status' => 'success',
              'data' => $orders
            ];

            return response()->json($data);
        } catch (\Exception $exception){
            $data = [
                'code' => '404',
                'status' => 'failed',
            ];

            return response()->json($data);
        }
    }

    public function update(StoreOrderPost $request, $id)
    {
        $validated = $request->validated();
        $order = Orders::find($id);
        try {

            $order->update($validated);
            $data = [
                'code' => '200',
                'status' => 'Update Order Successfully',

            ];

            return response()->json($data);
        } catch (\Exception $exception) {
            $data = [
                'code' => '400',
                'status' => 'Failed',
                'data' => $validated
            ];

            return response()->json($data);
        }
    }

    public function destroy($id)
    {
        $orders = Orders::find($id);
        try {
            $orders->delete();
            $data = [
                'code' => '400',
                'status' => 'Deleted Order Successfully',
            ];
            return response()->json($data);
        } catch (\Exception $exception) {
            $data = [
                'code' => '404',
                'status' => 'Deleted Order Failed',
            ];
            return response()->json($data);
        }
    }
}
